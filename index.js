// input: lay du lieu so ngay lam viec tu user
// math: tinh tong luong bang cach lay so ngay lam * 100000
// output: in ra man hinh so tien nhan duoc 
function tinhLuong() {
    var soNgay = document.getElementById("soNgay").value*1;
    var tongLuong = soNgay * 100000;
    document.getElementById("result1").innerHTML = `<h6 class="text-info"> Tổng lương tháng này là: ${tongLuong}`
}


/**input: tao form de lay du lieu 5 so thuc tu user
 * math: cong 5 so thuc va sau do chia /5 de ra so trung binh
    output: in ra man hinh so trung binh
*/
function tinhTrungBinh() {
    var num1 = document.getElementById("num1").value*1;
    var num2 = document.getElementById("num2").value*1;
    var num3 = document.getElementById("num3").value*1;
    var num4 = document.getElementById("num4").value*1;
    var num5 = document.getElementById("num5").value*1;
    var trungBinh = (num1+num2+num3+num4+num5) / 5;
    document.getElementById("result2").innerHTML = `<h6 class="text-info"> Số trung bình là: ${trungBinh}`
}


/**input: nhan data so luong USD ma user dang co
 * math: lay so ma user da nhap * 23500
 * output: in ra man hinh so tien user co bang VND
 **/
function doiTien() {
    var soUSD = document.getElementById("soUSD").value*1;
    var soVND = soUSD * 23500;
    if (soUSD >= 0 ) {
        document.getElementById("result3").innerHTML = `<h6 class="text-info"> Số tiền chuyển sang VND là: ${soVND}`
    }else{
        alert("Số tiền không thể là số âm")
    }
}


/**input: nguoi dung nhap chieu dai va chieu rong cua hinh 
 * math: tinh chu vi bang cach lay chieu dai + chieu rong sau do *2, tinh dien tich = chieu dai * chieu rong
 * output: in ra man hinh ket qua chu vi va dien tich
 */
function hinhHoc() {
    var chieuDai = document.getElementById("chieuDai").value*1;
    var chieuRong = document.getElementById("chieuRong").value*1;
    var chuVi = (chieuDai + chieuRong) * 2;
    var dienTich = chieuDai * chieuRong;
    document.getElementById("result4").innerHTML = `<h6 class="text-info"> Chu vi: ${chuVi}, Diện tích: ${dienTich}`;

}


/**input: nguoi dung nhap so co 2 chu so, neu nhap so khong hop le se bao loi
 * math: tim so hang don vi = input % 10, tim so hang chuc = (input - so hang don vi) / 10
 * ouput: in ra man hinh tong cua so hang don vi va so hang chuc
 */
function tongSo() {
    var sothuc = document.getElementById("numBer").value * 1;
    var soDonVi = sothuc % 10;
    var soHangChuc = (sothuc - soDonVi) / 10;
    var tongso = soDonVi + soHangChuc;
    if (sothuc < 0 || sothuc > 99){
        alert("Vui lòng nhập số dương có 2 chữ số");
    } else {
        document.getElementById("result5").innerHTML = `<h6 class="text-info"> Tổng 2 số là: ${tongso}`
    }
}
